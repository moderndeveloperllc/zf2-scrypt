ZF2 Scrypt Password Hashing Module
=============


# Abandonware
**Much like ZF2, this library is consigned to the history books. Use the much better and faster `password_hash()` in PHP with the PASSWORD_ARGON2ID algorithm.**



This class is an additional library (much like Zend\Crypt\Password\Bcrypt) used to generate scrypt password hashes that
are compatible with the [wg/scrypt](https://github.com/wg/scrypt) Java implementation. While it uses Zend\Crypt\Key\Derivation\Scrypt,
it also adds the parameters to the returned string that are required to compare password hashes. Also included is a
function to check the type of hash on your passwords. Useful if you are transitioning gradually from bcrypt to scrypt
 hashes.

NOTE: This is a simple ZF2 wrapper around moderndeveloperllc/scrypt. It provides a service called 'scrypt_password_hasher'
that can be called within a Zend Framework 2 application. If you are using a different framework, you may want to just
use the moderndeveloperllc/scrypt project in your composer.json file.
-------------

Installing via Composer
=======================
Install composer in a common location or in your project:

```bash
curl -s http://getcomposer.org/installer | php
```

Create the composer.json file as follows:

```json
{
    "require": {
        "moderndeveloperllc/zf2-scrypt" : "0.*"
    }
}
```

Run the composer installer:

```bash
php composer.phar install
```

Copy `vendor/moderndeveloperllc/zf2-scrypt/config/zf2-scrypt.local.php.dist` to `config/autoload/zf2-scrypt.local.php` in
your project. Optionally, change the parameters.

Finally, add the module name to your project's `config/application.config.php` under the `modules`
key:

    <?php
    return array(
        /* ... */
        'modules' => array(
            /* ... */
            'ModDev\Zf2Scrypt',
        ),
        /* ... */
    );

Usage
------------

### Hash a password

    <?php
    // In a class that is ServiceLocatorAwareInterface

    $scrypt = $this->getServiceLocator()->get('scrypt_password_hasher');
    $securePass = $scrypt->create('user password');

### Check the hashed password against an user input

    <?php
    // In a class that is ServiceLocatorAwareInterface

    $scrypt = $this->getServiceLocator()->get('scrypt_password_hasher');
    $securePass = 'the stored scrypt value';
    $password = 'the password to check';

    if ($scrypt->verify($password, $securePass)) {
        echo "The password is correct! \n";
    } else {
        echo "The password is NOT correct.\n";
    }

### Return the hash algorithm

    <?php
    // In a class that is ServiceLocatorAwareInterface

    $scrypt = $this->getServiceLocator()->get('scrypt_password_hasher');
    $passwordhashType = $scrypt->getHashType($hashedPassword);

### Check if the password needs rehashing

    <?php
    // In a class that is ServiceLocatorAwareInterface

    $scrypt = $this->getServiceLocator()->get('scrypt_password_hasher');
    echo $scrypt->needsRehash($hashedPassword);
