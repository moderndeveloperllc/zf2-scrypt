<?php
namespace ModDev\Zf2Scrypt\Factory;

use ModDev\Password\Scrypt;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ScryptFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        return new Scrypt($config['zf2-scrypt']);
    }
}
