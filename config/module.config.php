<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'scrypt_password_hasher'   => 'ModDev\Zf2Scrypt\Factory\ScryptFactory'
        )
    ),
);
